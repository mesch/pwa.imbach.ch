import Train from './train'

describe('@views/train', () => {
  it('is a valid view', () => {
    expect(Train).toBeAViewComponent()
  })
})
