import Flight from './flight'

describe('@views/flight', () => {
  it('is a valid view', () => {
    expect(Flight).toBeAViewComponent()
  })
})
