import Ship from './ship'

describe('@views/ship', () => {
  it('is a valid view', () => {
    expect(Ship).toBeAViewComponent()
  })
})
