import Notifications from './notifications'

describe('@views/notifications', () => {
  it('is a valid view', () => {
    expect(Notifications).toBeAViewComponent()
  })
})
