import Schedule from './schedule'

describe('@views/schedule', () => {
  it('is a valid view', () => {
    expect(Schedule).toBeAViewComponent()
  })
})
