import Infos from './infos'

describe('@views/infos', () => {
  it('is a valid view', () => {
    expect(Infos).toBeAViewComponent()
  })
})
