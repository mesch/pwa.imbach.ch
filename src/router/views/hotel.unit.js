import Hotel from './hotel'

describe('@views/hotel', () => {
  it('is a valid view', () => {
    expect(Hotel).toBeAViewComponent()
  })
})
