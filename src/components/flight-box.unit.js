import FlightBox from './flight-box'

describe('@components/flight-box', () => {
  it('exports a valid component', () => {
    expect(FlightBox).toBeAComponent()
  })
})
