import ServicesIcons from './services-icons'

describe('@components/services-icons', () => {
  it('exports a valid component', () => {
    expect(ServicesIcons).toBeAComponent()
  })
})
